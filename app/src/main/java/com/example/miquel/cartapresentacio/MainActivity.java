package com.example.miquel.cartapresentacio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends Activity {

    private Button bPresent;
    private Button bSalir;
    private Button bAcercaDe;
    private Button bFuncionamiento;
    private Button bVerPresentacio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bPresent = (Button) findViewById(R.id.Button02);
        bPresent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanzarPresentacion(null);
            }
        });

        bSalir = (Button) findViewById(R.id.bSalir);
        bSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        bAcercaDe = (Button) findViewById(R.id.Button04);
        bAcercaDe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanzarAcercaDe(null);
            }
        });

        bFuncionamiento = (Button) findViewById(R.id.Button03);
        bFuncionamiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lanzarFuncionamineto(null);
            }
        });
        bVerPresentacio = (Button) findViewById(R.id.Button01);
        bVerPresentacio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lanzarVerPresentacion(null);
            }
        });
    }

    public void lanzarVerPresentacion(View view){
        Intent i = new Intent(this, Resultat.class);
    }

    public void lanzarAcercaDe (View view){
        Intent i = new Intent(this, AcercaDe.class);
        startActivity(i);
    }

    public void lanzarPresentacion (View view){
        Intent i = new Intent(this, Presentacion.class);
        startActivity(i);
    }
    public void lanzarFuncionamineto (View view){
        Intent i = new Intent (this, Funcionamiento.class);
        startActivity(i);
    }

    public void salir(View view){
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}