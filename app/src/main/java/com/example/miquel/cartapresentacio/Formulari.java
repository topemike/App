package com.example.miquel.cartapresentacio;

/**
 * Created by Miquel on 02/07/2015.
 */
public class Formulari {

    private String nombre;
    private String presentacion;
    private String departamento;
    private String direccion;
    private String empresa;
    private String poblacion;
    private String provincia;
    private String foto;
    private int postalcode;
    private int telfFijo;
    private int telfMovil;
    private String mail;

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public int getTelfFijo() {
        return telfFijo;
    }

    public void setTelfFijo(int telfFijo) {
        this.telfFijo = telfFijo;
    }

    public int getTelfMovil() {
        return telfMovil;
    }

    public void setTelfMovil(int telfMovil) {
        this.telfMovil = telfMovil;
    }

    public int getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(int postalcode) {
        this.postalcode = postalcode;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
