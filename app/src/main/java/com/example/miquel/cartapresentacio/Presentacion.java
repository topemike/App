package com.example.miquel.cartapresentacio;

        import android.app.Activity;
        import android.app.AlertDialog;
        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.net.Uri;
        import android.os.Bundle;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.Toast;

/**
 * Created by Miquel on 06/07/2015.
 */
public class Presentacion extends Activity {

    private ImageView imageView;
    private Formulari formulari;

    EditText prsEmpresa,name,dept,location,empresa,postalCode,poblacion,provincia,numFix,numMovil,email;
    Button btnGuar;
    Button btnCan;

    final static int RESULTADO_EDITAR=1;
    final static int RESULTADO_GALERIA=2;
    final static int RESULTADO_FOTO=3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.portada);

        imageView = (ImageView) findViewById(R.id.fotoEmpresa);
        imageView = (ImageView) findViewById(R.id.fotopersonal);

        prsEmpresa = (EditText) findViewById(R.id.editTextPresentacioEmpresa);
        name = (EditText) findViewById(R.id.editTextNombre);
        dept = (EditText) findViewById(R.id.editTextDepartamento);
        location = (EditText) findViewById(R.id.editTextDireccion);
        postalCode = (EditText) findViewById(R.id.editTextCodigoPostal);
        empresa = (EditText) findViewById(R.id.editTextEmpresa);
        poblacion = (EditText) findViewById(R.id.editTextPoblacion);
        provincia = (EditText) findViewById(R.id.editTextProvincia);
        numFix = (EditText) findViewById(R.id.editTextFijo);
        numMovil = (EditText) findViewById(R.id.editTextMovil);
        email = (EditText) findViewById(R.id.editTextMail);

        btnGuar = (Button) findViewById(R.id.buttonGuardar);
        btnCan = (Button) findViewById(R.id.buttonCancelar);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void galeria(View view){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(intent, RESULTADO_GALERIA);
    }

    public void galeriafoto(View view){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(intent, RESULTADO_GALERIA);
    }

    protected void ponerFoto(ImageView imageView, String uri){
        if (uri != null){
            imageView.setImageURI(Uri.parse(uri));
        }else{
            imageView.setImageBitmap(null);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode==RESULTADO_EDITAR){

        }else if (requestCode == RESULTADO_GALERIA && resultCode == Activity.RESULT_OK){
            formulari.setFoto(data.getDataString());
            ponerFoto(imageView, formulari.getFoto());

        }
        else if(requestCode == RESULTADO_FOTO && resultCode == Activity.RESULT_OK) {
            formulari.setFoto(data.getDataString());
            ponerFoto(imageView, formulari.getFoto());
        }

    }

    public void saveForm(View v){

        SharedPreferences.Editor prefs = getSharedPreferences("form", Context.MODE_PRIVATE).edit();
        prefs.putString("prsEmpresa", prsEmpresa.getText().toString());
        prefs.putString("name", name.getText().toString());
        prefs.putString("dept", dept.getText().toString());
        prefs.putString("location", location.getText().toString());
        prefs.putString("empresa", empresa.getText().toString());
        prefs.putString("postalCode", postalCode.getText().toString());
        prefs.putString("poblacion", poblacion.getText().toString());
        prefs.putString("provincia", provincia.getText().toString());
        prefs.putString("numFix", numFix.getText().toString());
        prefs.putString("numMovil", numMovil.getText().toString());
        prefs.putString("email", email.getText().toString());
        prefs.commit();
        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
    }

}