package com.example.miquel.cartapresentacio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by usuario on 09/07/2015.
 */
public class Verifica extends Activity {
    private TextView tv;
    private Button btnAccept;
    private Button btnCancel;
    private String retorno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pregunta);

        tv = (TextView) findViewById(R.id.tv1);
        btnAccept = (Button) findViewById(R.id.btn1);
        btnCancel = (Button) findViewById(R.id.btn2);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retorno="ACCEPT";
                Salir();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retorno="CANCEL";
                finalizar();
            }
        });

    }

    public void finalizar(){
        Intent i = new Intent();
        i.putExtra("resultado", retorno);
        setResult(RESULT_OK, i);
        finish();
    }
    public void Salir(){
        finish();
    }
}
